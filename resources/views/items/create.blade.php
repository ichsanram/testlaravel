@extends('layout.main')

@section('titile', 'Form Tambah Data Barang')

@section('container')
	<div class="container">
		<div class="row">
			<div class="col-8">
				<h1 class="mt-2">Form Tambah Data Barang</h1>

					<form method="post" action="/items">
						@csrf
					  <div class="form-group">
					    <label for="nama_barang">Nama Barang</label>
					    <input type="text" class="form-control @error ('nama_barang') is-invalid @enderror"  id="nama_barang" placeholder="Masukan Nama Barang" name="nama_barang" value="{{old('nama_barang')}}">
					    <div class="invalid-feedback">
					    	@error('nama_barang')
					    	{{$message}}
					    	@enderror
				        </div>
					  </div>
					  <button type="submit" class="btn btn-primary">Tambah Data</button>
					</form>
					
			</div>
		</div>
	</div>
@endsection
    
